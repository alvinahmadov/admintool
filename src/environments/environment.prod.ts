// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import { Environment } from './model';

export const environment: Environment = {
	production:              true,
	HTTP_SERVICES_ENDPOINT:  "http://spa.papaya-develop.moos.solutions/es",
	HTTPS_SERVICES_ENDPOINT: "https://spa.papaya-develop.moos.solutions/es",
	LOCALE:                  "ru-RU",
	DATETIME_FORMAT:         "dd/MM/yyyy hh:mm:ss",
	TIMEZONE:                "Europe/Moscow",
	HOST:                    "localhost",
	PORT:                    4200,
	WEB_CONCURRENCY:         1,
	WEB_MEMORY:              1024,
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
