import { Pipe, PipeTransform } from '@angular/core';
import { IUser }               from 'app/common/interfaces';

@Pipe({
	      name: 'userdata'
      })
export class UserPipe implements PipeTransform
{
	transform(value: IUser): string
	{
		if(value)
			return `@${value.firstName ?? value.lastName}`;
		else
			return '';
	}
}
