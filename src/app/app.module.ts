import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { registerLocaleData }      from '@angular/common';
import ru                          from '@angular/common/locales/ru';
import { FormsModule }             from '@angular/forms';
import { HttpClientModule }        from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NZ_I18N, ru_RU }          from 'ng-zorro-antd/i18n';
import { NzLayoutModule }          from 'ng-zorro-antd/layout';
import { NzMenuModule }            from 'ng-zorro-antd/menu';
import { NzToolTipModule }         from 'ng-zorro-antd/tooltip';
import { NzInputModule }           from 'ng-zorro-antd/input';
import { RestApiService }          from 'services/rest-api.service';

import { AppRoutingModule }    from 'app/app-routing.module';
import { AppComponent }        from 'app/app.component';
import { IconsProviderModule } from 'app/icons-provider.module';
import { DbService }           from "services/db.service";
import { NzModalModule }       from "ng-zorro-antd/modal";
import { NzButtonModule }      from "ng-zorro-antd/button";
import { NzBackTopModule }     from "ng-zorro-antd/back-top";

registerLocaleData(ru);

@NgModule({
	          declarations: [
		          AppComponent
	          ],
	          imports:      [
		          BrowserModule.withServerTransition({ appId: 'serverApp' }),
		          AppRoutingModule,
		          FormsModule,
		          HttpClientModule,
		          BrowserAnimationsModule,
		          IconsProviderModule,
		          NzLayoutModule,
		          NzMenuModule,
		          NzToolTipModule,
		          NzInputModule,
		          NzModalModule,
		          NzButtonModule,
		          NzBackTopModule
	          ],
	          providers:    [
		          { provide: NZ_I18N, useValue: ru_RU },
		          RestApiService,
		          DbService
	          ],
	          bootstrap:    [AppComponent]
          })
export class AppModule {}
