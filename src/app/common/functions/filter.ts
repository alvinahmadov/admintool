import { ITableData, ITreeNode } from 'app/common/interfaces';
import { FilteredTreeResult }    from 'app/common/classes';

export function filterTableData(data: ITableData[], value: string)
{
	const _filter = (table: ITableData, result: ITableData[]) =>
	{
		if(table.name.toLowerCase().search(value) !== -1)
		{
			result.push(table);
			return result;
		}
		if(table.value.toLowerCase().search(value) !== -1)
		{
			result.push(table);
			return result;
		}
		return result;
	}
	
	return data.reduce((a, b) => _filter(b, a), [] as ITableData[]);
}

export function filterTreeData(data: ITreeNode[], value: string): FilteredTreeResult
{
	const needsToExpanded = new Set<ITreeNode>();
	const _filter = (node: ITreeNode, result: ITreeNode[]) =>
	{
		if(node.name.search(value) !== -1)
		{
			result.push(node);
			return result;
		}
		if(Array.isArray(node.children))
		{
			const nodes = node.children.reduce((a, b) => _filter(b, a), [] as ITreeNode[]);
			if(nodes.length)
			{
				const parentNode = { ...node, children: nodes };
				needsToExpanded.add(parentNode);
				result.push(parentNode);
			}
		}
		return result;
	};
	const treeData = data.reduce((a, b) => _filter(b, a), [] as ITreeNode[]);
	return new FilteredTreeResult(treeData, [...needsToExpanded]);
}

export function filterTableSearch(inputMap: Map<string, ITableData[]>, value: string)
{
	let outputMap: Map<string, ITableData[]> = new Map<string, ITableData[]>();
	value = value.toLowerCase();
	
	for(const [key, table] of inputMap)
	{
		const res = filterTableData(table, value);
		if(res.length > 0)
		{
			console.log(res)
			outputMap.set(key, res);
		}
	}
	
	return outputMap;
}

export function filterTreeSearch(items: Map<string, ITreeNode[]>, searchValue: string)
{
	const searchText = searchValue.toLowerCase();
	
	let resultMap: Map<string, ITreeNode[]> = new Map<string, ITreeNode[]>();
	
	for(const [key, nodes] of items)
	{
		resultMap.set(key, filterTreeData(nodes, searchText).treeData);
	}
	return resultMap;
}
