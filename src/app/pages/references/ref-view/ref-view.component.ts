import { AfterViewInit, Component, OnInit }                   from '@angular/core';
import { ActivatedRoute }                                     from '@angular/router';
import { BehaviorSubject, Observable, Subject, Subscription } from 'rxjs';
import { switchMap, takeUntil }                               from 'rxjs/operators';
import { RestApiService }                                     from 'services/rest-api.service';
import { ITableData, ITreeNode, IReferenceTypes }             from 'app/common/interfaces';
import { parseResponse, filterTableSearch }                   from 'app/common/functions';

@Component({
	           selector:    'fz-ref-view',
	           templateUrl: './ref-view.component.html',
	           styleUrls:   ['./ref-view.component.scss']
           })
export class RefViewComponent implements OnInit, AfterViewInit
{
	public viewMode: "tree" | "table" = "table";
	public tableDataMap: Map<string, ITableData[]> = new Map<string, ITableData[]>();
	public activeDataMap: Map<string, ITableData[]> = new Map<string, ITableData[]>();
	
	public treeNodesMap: Map<string, ITreeNode[]> = new Map<string, ITreeNode[]>();
	public currentItems: Map<string, ITreeNode[]> = new Map<string, ITreeNode[]>();
	public treeData$: Observable<IReferenceTypes>;
	public referenceData$: Subscription;
	public searchValue$: BehaviorSubject<string> = new BehaviorSubject<string>('');
	
	public searchText = ''
	
	public totalItems: number;
	
	public currentPage: number = 0;
	public itemsPerPage: number = 10;
	public totalPages: number;
	private ngDestroy$: Subject<boolean> = new Subject<boolean>();
	
	public refType: string;
	
	public isActive: boolean = false;
	public showPagination: boolean = true;
	
	private startPage = () => this.currentPage * this.itemsPerPage;
	
	private endPage = () => (this.currentPage + 1) * this.itemsPerPage;
	
	constructor(
			private route: ActivatedRoute,
			private apiService: RestApiService,
	)
	{}
	
	ngOnInit(): void
	{
		this.treeData$ = this.apiService.getReferenceTypes();
		this.loadData();
		this.route
		    .paramMap
		    .pipe(
				    switchMap(params => params.getAll('type'))
		    )
		    .subscribe(reftype => this.refType = reftype);
		
	}
	
	public ngAfterViewInit(): void
	{
		this.loadData();
		this.searchValue$.subscribe(
				value =>
				{
					if(value.length <= 2)
					{
						this.reset();
						return;
					}
					delete this.activeDataMap
					this.activeDataMap = filterTableSearch(this.tableDataMap, value);
					
					if(!this.searchText && !this.activeDataMap.size)
					{ this.reset(); }
					else
					{ this.showPagination = false; }
				})
	}
	
	onPageChange(currentPage: number)
	{
		this.currentPage = currentPage - 1;
		this.reset();
	}
	
	reset()
	{
		delete this.activeDataMap;
		
		this.activeDataMap = new Map(
				Array.from(this.tableDataMap)
				     .sort()
				     .slice(this.startPage(), this.endPage())
		)
	}
	
	clearSearch()
	{
		this.reset();
		this.searchText = '';
		this.showPagination = true;
	}
	
	onPageSizeChange(size: number)
	{
		this.itemsPerPage = size;
	}
	
	activate(b: boolean)
	{
		this.isActive = b;
	}
	
	private loadData()
	{
		let nodeKey = 0;
		let treeNodes: ITreeNode[] = [];
		this.referenceData$ =
				this.apiService
				    .searchReference(this.refType)
				    .pipe(takeUntil(this.ngDestroy$))
				    .subscribe(
						    ref =>
						    {
							    this.totalItems = ref.hits.total;
							    this.isActive = (this.itemsPerPage >= this.totalPages);
							    let result = ref.hits.hits;
							    parseResponse(
									    result,
									    treeNodes,
									    null,
									    ++nodeKey
							    );
							    this.totalPages = (treeNodes.length / this.itemsPerPage) * 10;
							    treeNodes.forEach(
									    node =>
									    {
										    let key = node.children.find(n => n.name === '_id').value
										    let sourceNode = node.children.find(n => n.name === '_source');
										    if(key)
										    {
											    if(this.viewMode === "table")
											    {
												    if(sourceNode)
												    {
													    let tableData: ITableData[] = []
													
													    sourceNode
															    .children
															    .forEach(node => tableData
																	    .push({
																		          name:  node.name,
																		          value: node.value
																	          }));
													
													    this.tableDataMap
													        .set(key, tableData);
												    }
											    }
											    else
											    {
												    this.treeNodesMap
												        .set(key, node.children);
											    }
										    }
									    }
							    );
							    this.onPageChange(1);
						    });
	}
}
