interface IReferenceTypeProperty
{
	[arg: string]: IReferenceTypeProperty;
}

interface IReferenceTypeMapping
{
	name?: string;
	dynamic?: string;
	properties?: IReferenceTypeProperty;
}

interface IReferenceType
{
	aliases?: object;
	mappings?: IReferenceTypeMapping;
	settings?: object;
}

export interface IReferenceTypes
{
	ref: IReferenceType;
}
