export interface IShard
{
	total: number;
	successfull: number;
	skipped: number;
	failed: number;
}
