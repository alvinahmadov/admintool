import { ITreeNode } from 'app/common/interfaces';

export class FilteredTreeResult
{
	constructor(
			public treeData: ITreeNode[],
			public needsToExpanded: ITreeNode[] = []
	)
	{}
}

export default FilteredTreeResult;
