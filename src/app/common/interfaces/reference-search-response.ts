export interface IReferenceSearchResponse
{
	_id: string,
	_type: string,
	_index: string,
	_version: number,
	found: boolean,
	_source: {
		[arg: string]: any
	};
}
