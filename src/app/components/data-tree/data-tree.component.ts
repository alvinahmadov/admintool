import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import { SelectionModel }                          from '@angular/cdk/collections';
import { FlatTreeControl }                         from '@angular/cdk/tree';
import { NzModalService }                          from 'ng-zorro-antd/modal';
import { NzTreeFlatDataSource, NzTreeFlattener }   from 'ng-zorro-antd/tree-view';

import { IFlatNode, ITreeNode } from 'app/common/interfaces';
import { RestApiService }       from 'services/rest-api.service';

@Component({
	           selector:    'fz-data-tree',
	           templateUrl: './data-tree.component.html',
	           styleUrls:   ['./data-tree.component.scss']
           })
export class DataTreeComponent implements OnInit, AfterViewInit
{
	@Input()
	data: ITreeNode[];
	
	/**
	 * Id of reference item
	 * */
	@Input()
	id: string;
	
	/**
	 * Type of reference item
	 * */
	@Input()
	type: string;
	
	public flatNodeMap = new Map<IFlatNode, ITreeNode>();
	public nestedNodeMap = new Map<ITreeNode, IFlatNode>();
	public selectListSelection = new SelectionModel<IFlatNode>(true);
	
	private transformer = (node: ITreeNode, level: number) =>
	{
		const existingNode = this.nestedNodeMap.get(node);
		const flatNode = existingNode && existingNode.key === node.key
		                 ? existingNode
		                 : {
					expandable: !!node.children && node.children.length > 0,
					name:       node.name,
					level,
					key:        node.key,
					value:      node.value
				};
		flatNode.name = node.name;
		
		this.flatNodeMap.set(flatNode, node);
		this.nestedNodeMap.set(node, flatNode);
		
		return flatNode;
	};
	
	public treeControl: FlatTreeControl<IFlatNode, ITreeNode> = new FlatTreeControl(
			node => node.level,
			node => node.expandable
	);
	
	public treeFlattener: NzTreeFlattener<ITreeNode, IFlatNode, ITreeNode> = new NzTreeFlattener(
			this.transformer,
			node => node.level,
			node => node.expandable,
			node => node.children,
	);
	
	public dataSource: NzTreeFlatDataSource<ITreeNode, IFlatNode, ITreeNode> = new NzTreeFlatDataSource(
			this.treeControl,
			this.treeFlattener
	);
	
	constructor(
			private apiService: RestApiService,
			private modalService: NzModalService
	)
	{}
	
	public ngOnInit(): void
	{
		
		this.dataSource.setData(this.data);
	}
	
	public ngAfterViewInit(): void
	{
		this.treeControl.expandAll();
	}
	
	public hasChild = (_, node: IFlatNode) => node.expandable;
	public hasChildren = (_, node: IFlatNode) => this.flatNodeMap.get(node).children.length > 0;
	public hasNoContent = (_, node: IFlatNode) => node.name === '';
	public hasParent = (node: IFlatNode) => node.level !== 0
	public hasValue = (_, node: IFlatNode) => this.flatNodeMap.get(node).value !== undefined;
	public trackBy = (_, node: IFlatNode) => `${node.key}-${node.name}`;
	
	public addNewNode(node: IFlatNode): void
	{
		let parentNode = this.flatNodeMap.get(node);
		if(parentNode)
		{
			parentNode.children = parentNode.children || [];
			parentNode.children.push({
				                         name: '',
				                         key:  `${parentNode.key}-${parentNode.children.length}`
			                         });
			this.dataSource.setData(this.data);
			this.treeControl.expand(node);
		}
	}
	
	public async deleteNode(node: IFlatNode): Promise<void>
	{
		this._confirm(
				async() => await this._delete(node),
				`Элемент '${node.value ?? node.name}' будет удален безвозвратно`
		);
	}
	
	public getValue(node: IFlatNode)
	{
		const nestedNode = this.flatNodeMap.get(node);
		if(nestedNode)
			return nestedNode.value;
		
		return ''
	}
	
	public saveNode(
			node: IFlatNode,
			name?: string,
			value?: string
	): void
	{
		if(this.selectListSelection.isSelected(node))
		{
			this.selectListSelection.toggle(node);
		}
		if(!name && !value)
			return;
		
		const nestedNode = this.flatNodeMap.get(node);
		if(nestedNode)
		{
			nestedNode.name = name ?? nestedNode.name;
			nestedNode.value = value ?? nestedNode.value;
			this.dataSource.setData(this.data);
			this.editData();
		}
	}
	
	public cancel(node: IFlatNode)
	{
		this.selectListSelection.toggle(node);
	}
	
	private _confirm(
			okCallback: () => any,
			content?: string,
	)
	{
		return this.modalService
		           .confirm(
				           {
					           nzTitle:       "Вы уверены?",
					           nzContent:     content ?? "",
					           nzOkText:      "Подтвердить",
					           nzCancelText:  "Отменить",
					           nzCentered:    true,
					           nzClosable:    true,
					           nzNoAnimation: true,
					           nzOnOk:        okCallback,
				           },
				           "warning"
		           );
	}
	
	private editData()
	{
		let parentData = this.data.filter(node => node?.children !== undefined && node.children.length > 0);
		
		if(parentData)
		{
			let rawData: string = "";
			if(parentData.length === 1)
			{
				let parent = parentData[0]
				const childCount = parent.children.length;
				parent.children
				      .forEach(
						      (n, i) => rawData += `"${n.name}": "${n.value}" ${i < childCount - 1 ? ',' : ''}`
				      );
				
				const data = JSON.parse(`{${rawData}}`);
				this.apiService.editReference(this.type, this.id, data)
				    .subscribe(res => console.log(res));
			}
		}
	}
	
	private async _delete(node: IFlatNode): Promise<void>
	{
		const originNode = this.flatNodeMap.get(node);
		
		const dfsParentNode = (): ITreeNode | null =>
		{
			const stack = [...this.data];
			while(stack.length > 0)
			{
				const n = stack.pop()!;
				if(n.children)
				{
					if(n.children.find(e => e === originNode))
					{
						return n;
					}
					
					for(let i = n.children.length - 1; i >= 0; i--)
					{
						stack.push(n.children[i]);
					}
				}
			}
			return null;
		};
		
		const parentNode = dfsParentNode();
		if(parentNode && parentNode.children)
		{
			parentNode.children = parentNode.children.filter(e => e !== originNode);
		}
		
		this.dataSource.setData(this.data);
		this.editData();
	}
}
