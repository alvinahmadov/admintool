import { Component, OnInit } from '@angular/core';
import { IUser }             from "app/common/interfaces";
import { DbService }         from "services/db.service";
import { Router }            from "@angular/router";

@Component({
	           selector:    'app-root',
	           templateUrl: './app.component.html',
	           styleUrls:   ['./app.component.scss']
           })
export class AppComponent implements OnInit
{
	isCollapsed = false;
	isVisible = false;
	isConfirmLoading = false;
	hasErrors = false;
	user: IUser;
	title: string = 'farzoom-refbook';
	
	constructor(
			private router: Router,
			private dbService: DbService
	)
	{}
	
	public ngOnInit(): void
	{
		this.user = {
			firstName: '',
			lastName:  '',
			loginDate: new Date()
		};
		
		if(!sessionStorage.getItem('user'))
		{
			this.isVisible = true;
		}
		else
		{
			let usr: IUser = JSON.parse(sessionStorage.getItem('user'));
			this.user.firstName = usr.firstName;
			this.user.lastName = usr.lastName;
		}
	}
	
	checkValid()
	{
		return !(this.hasErrors = !this.user.firstName);
	}
	
	handleOk(): void
	{
		if(!this.checkValid())
			return;
		
		this.isConfirmLoading = true;
		sessionStorage.setItem('user', JSON.stringify(this.user));
		this.dbService.add({
			                   message: "Вход в систему",
			                   author:  this.user,
			                   level:   'INFO'
		                   });
		setTimeout(() =>
		           {
			           this.isVisible = false;
			           this.isConfirmLoading = false;
			           this.router.navigateByUrl('/');
		           }, 1000);
	}
}
