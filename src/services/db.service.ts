import { Injectable }  from '@angular/core';
import { ILogMessage } from 'app/common/interfaces';
import { LogsDB }      from 'app/common/classes/logs-db';

@Injectable({
	            providedIn: 'root'
            })
export class DbService
{
	private db: LogsDB;
	
	constructor()
	{
		this.db = new LogsDB('ref-logs-db');
	}
	
	public async add(log: ILogMessage)
	{
		if(!log.author)
		{
			log.author = JSON.parse(sessionStorage.getItem('user'));
		}
		return this.db.addItem(log);
	}
	
	public async get(id: number)
	{
		return this.db.getItem(id);
	}
	
	public async getAll(findObj = {})
	{
		return this.db.getAll(findObj)
	}
	
}
