export interface IReference
{
	_id: string;
	_index: string;
	_type: string;
	_score: number;
	_source: {
		[arg: string]: any
	};
}

export default IReference;
