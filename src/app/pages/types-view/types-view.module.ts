import { NgModule }     from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }  from '@angular/forms';

import { NzButtonModule }     from 'ng-zorro-antd/button';
import { NzCollapseModule }   from 'ng-zorro-antd/collapse';
import { NzInputModule }      from 'ng-zorro-antd/input';
import { NzModalService }     from 'ng-zorro-antd/modal';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { NzTreeViewModule }   from 'ng-zorro-antd/tree-view';

import { IconsProviderModule } from 'app/icons-provider.module';
import { DataTreeModule }      from 'app/components/data-tree/data-tree.module';
import { TypesViewComponent }  from 'app/pages/types-view/types-view.component';

@NgModule({
	          declarations: [
		          TypesViewComponent,
	          ],
	          imports:      [
		          CommonModule,
		          FormsModule,
		          IconsProviderModule,
		          DataTreeModule,
		          NzButtonModule,
		          NzInputModule,
		          NzTreeViewModule,
		          NzCollapseModule,
		          NzPaginationModule
	          ],
	          providers:    [NzModalService]
          })
export class TypesViewModule {}
