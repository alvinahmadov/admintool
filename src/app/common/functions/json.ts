import { ITreeNode } from 'app/common/interfaces';

export function parseResponse(
		jsonobj: any,
		treeNodes: ITreeNode[],
		parentNode?: ITreeNode,
		depth: number = 0
)
{
	
	// if(Array.isArray(jsonobj))
	// {
	// 	for(const el of jsonobj)
	// 	{
	// 		parseResponse(el, treeNodes);
	// 	}
	// 	return;
	// }
	
	if(typeof jsonobj === 'object')
	{
		for(const key of Object.keys(jsonobj))
		{
			const value = jsonobj[key];
			
			if(value && typeof value === "string")
			{
				treeNodes.push({
					               name:  key,
					               key:   `${depth + 1}-${treeNodes.length}`,
					               value: value
				               });
			}
			else if(value && typeof value === 'object')
			{
				let treeNode: ITreeNode = {
					name:     key,
					key:      `${depth}-0`,
					children: []
				}
				
				treeNodes.push(treeNode);
				parseResponse(value, treeNode.children, treeNode, ++depth);
			}
			// else if(Array.isArray(value))
			// {
			// 	parseResponse(value, treeNodes, parentNode, depth);
			// }
		}
	}
	
	return jsonobj;
}
