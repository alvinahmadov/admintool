export interface IUser
{
	firstName: string;
	lastName?: string;
	loginDate: Date
}
