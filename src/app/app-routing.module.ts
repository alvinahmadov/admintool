import { NgModule }             from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
	{
		path:         'ref',
		loadChildren: () => import('app/pages/references/references.module')
				.then(m => m.ReferencesModule)
	},
	{
		path:         'logs',
		loadChildren: () => import('./pages/logs-view/logs-view.module')
				.then(m => m.LogsViewModule).catch(err => console.error(err))
	}
];

@NgModule({
	          imports: [
		          RouterModule.forRoot(routes, {
			          initialNavigation: "enabled",
			          onSameUrlNavigation: "reload",
		          })
	          ],
	          exports: [RouterModule]
          })
export class AppRoutingModule {}
