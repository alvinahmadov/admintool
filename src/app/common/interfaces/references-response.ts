import { IShard }     from './shard';
import { IReference } from './reference';

export interface IReferencesSearchResponse
{
	took: number;
	timed_out: boolean;
	_shards: IShard;
	hits: {
		total: number;
		max_score: number;
		hits: IReference[]
	};
}
