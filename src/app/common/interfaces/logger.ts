import { IUser } from './user';

export type TLogLevel = "LOG" | "INFO" | "WARN" | "ERROR" | "SUCCESS";

export interface ILogMessage
{
	message: string;
	author?: IUser;
	level?: 'LOG' | 'INFO' | 'WARN' | 'ERR' | 'SUCCESS' | string;
	timestamp?: string;
	createdAt?: Date | string;
}
