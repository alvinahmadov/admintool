import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'normalize' })
export class NodeNamePipe implements PipeTransform
{
	transform(value: string): string
	{
		return value.replace(/_/gi, '');
	}
}
