import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { share }      from 'rxjs/operators';

import {
	IReferenceTypes,
	IReferencesSearchResponse,
	IReferenceSearchResponse
}                      from 'app/common/interfaces';
import { environment } from 'environments/environment';

@Injectable({
	            providedIn: 'root'
            })
export class RestApiService
{
	private apiUrl = environment.HTTP_SERVICES_ENDPOINT;
	
	constructor(private http: HttpClient)
	{
	}
	
	public getReferenceTypes(): Observable<IReferenceTypes>
	{
		return this.http
		           .get<IReferenceTypes>(this.endpoint('/ref'))
		           .pipe(share());
	}
	
	public searchReference(refType: string, size: number = 10000): Observable<IReferencesSearchResponse>
	{
		return this.http
		           .get<IReferencesSearchResponse>(
				           this.endpoint(`/ref/${refType}/_search?size=${size}`)
		           )
		           .pipe(share())
	}
	
	public getReference(refType: string, refId: string): Observable<IReferenceSearchResponse>
	{
		return this.http
		           .get<IReferenceSearchResponse>(
				           this.endpoint(`/ref/${refType}/${refId}`)
		           )
		           .pipe(share());
	}
	
	public editReference(refType: string, refId: string, data: object)
	{
		return this.http.post(
				this.endpoint(`/ref/${refType}/${refId}`),
				data,
				{ withCredentials: false });
	}
	
	private endpoint(path: string)
	{
		if(path.startsWith('/', 0))
			return `${this.apiUrl}${path}`;
		else
			return `${this.apiUrl}/${path}`;
	};
}
