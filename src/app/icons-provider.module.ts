import { NgModule }               from '@angular/core';
import { NZ_ICONS, NzIconModule } from 'ng-zorro-antd/icon';

import {
	MenuFoldOutline,
	MenuUnfoldOutline,
	FormOutline,
	DashboardOutline,
	MinusOutline,
	PlusOutline,
} from '@ant-design/icons-angular/icons';

const icons = [
	MenuFoldOutline,
	MenuUnfoldOutline,
	DashboardOutline,
	FormOutline,
	PlusOutline,
	MinusOutline
];

@NgModule({
	          imports:   [NzIconModule],
	          exports:   [NzIconModule],
	          providers: [
		          { provide: NZ_ICONS, useValue: icons }
	          ]
          })
export class IconsProviderModule
{
}
