import { Dexie }       from 'dexie';
import { ILogMessage } from 'app/common/interfaces'

export class LogsDB extends Dexie
{
	public logs: Dexie.Table<ILogMessage, number>;
	
	constructor(dbname: string)
	{
		super(dbname);
		this.version(1)
		    .stores({
			            logs: "++id,message,level,author,createdAt,timestamp"
		            });
		
		this.logs = this.table('logs');
	}
	
	addItem(log: ILogMessage): Promise<number>
	{
		return this.logs.add({
			                     message:   log.message,
			                     level:     log.level ?? 'INFO',
			                     author:    log.author,
			                     timestamp: LogsDB._timestamp,
			                     createdAt: LogsDB._createdAt
		                     });
	}
	
	async getItem(id: number): Promise<ILogMessage>
	{
		return this.logs.get(id);
	}
	
	async getAll(
			findObj: Partial<ILogMessage> = {}
	): Promise<ILogMessage[]>
	{
		if(findObj.author)
		{
			return this.logs.where({ author: findObj.author }).toArray();
		}
		else if(findObj.level)
		{
			return this.logs.where({ level: findObj.level }).toArray();
		}
		else if(findObj.createdAt)
		{
			return this.logs.where({ createdAt: findObj.createdAt }).toArray();
		}
		else return this.logs.toArray();
	}
	
	private static get _timestamp()
	{
		return (new Date()).toTimeString();
	}
	
	private static get _createdAt()
	{
		return new Date();
	}
	
}
