import { NgModule }            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { FormsModule }         from '@angular/forms';
import { Route, RouterModule } from '@angular/router';

import { NzButtonModule }      from 'ng-zorro-antd/button';
import { NzBreadCrumbModule }  from 'ng-zorro-antd/breadcrumb';
import { NzNoAnimationModule } from 'ng-zorro-antd/core/no-animation';
import { NzCollapseModule }    from 'ng-zorro-antd/collapse';
import { NzDividerModule }     from 'ng-zorro-antd/divider';
import { NzFormModule }        from 'ng-zorro-antd/form';
import { NzInputModule }       from 'ng-zorro-antd/input';
import { NzListModule }        from 'ng-zorro-antd/list';
import { NzModalService }      from 'ng-zorro-antd/modal';
import { NzPaginationModule }  from 'ng-zorro-antd/pagination';
import { NzPopconfirmModule }  from 'ng-zorro-antd/popconfirm';

import { DetailViewComponent } from './detail-view/detail-view.component';
import { IconsProviderModule } from 'app/icons-provider.module';
import { DataTreeModule }      from 'app/components/data-tree/data-tree.module';
import { ReferencesComponent } from 'app/pages/references/references.component';
import { RefViewComponent }    from 'app/pages/references/ref-view/ref-view.component';
import { DataTableModule }     from 'app/components/data-table/data-table.module';
import { RestApiService }      from 'services/rest-api.service';

const routes: Route[] = [
	{
		path:      '',
		component: ReferencesComponent
	},
	{
		path:      'type/:type',
		component: RefViewComponent
	},
	{
		path:      'type/:type/:id',
		component: DetailViewComponent
	}
]

@NgModule({
	          imports: [
		          CommonModule,
		          DataTreeModule,
		          DataTableModule,
		          RouterModule.forChild(routes),
		          NzDividerModule,
		          NzFormModule,
		          NzButtonModule,
		          IconsProviderModule,
		          NzCollapseModule,
		          NzPaginationModule,
		          NzPopconfirmModule,
		          NzInputModule,
		          FormsModule,
		          NzListModule,
		          NzBreadCrumbModule,
		          NzNoAnimationModule,
		          DataTableModule
	          ],
	          declarations: [
		          ReferencesComponent,
		          RefViewComponent,
		          DetailViewComponent
	          ],
	          exports:      [
		          ReferencesComponent
	          ],
	          providers:    [
		          RestApiService,
		          NzModalService
	          ]
          })
export class ReferencesModule {}
