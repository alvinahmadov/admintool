export interface ITreeNode
{
	name: string;
	key: string;
	value?: string;
	children?: ITreeNode[];
}

export default ITreeNode;
