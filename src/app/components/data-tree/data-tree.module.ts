import { NgModule }     from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }  from '@angular/forms';

import { NzButtonModule }     from 'ng-zorro-antd/button';
import { NzInputModule }      from 'ng-zorro-antd/input';
import { NzTreeViewModule }   from 'ng-zorro-antd/tree-view';
import { NzTableModule }      from 'ng-zorro-antd/table';
import { NzPopconfirmModule } from 'ng-zorro-antd/popconfirm';

import { IconsProviderModule } from 'app/icons-provider.module';
import { NodeNamePipe }        from 'app/pipes/nodename.pipe';
import { DataTreeComponent }   from './data-tree.component';

@NgModule({
	          declarations: [
		          DataTreeComponent,
		          NodeNamePipe
	          ],
	          exports:      [
		          DataTreeComponent,
		          NodeNamePipe
	          ],
	          imports:      [
		          CommonModule,
		          FormsModule,
		          IconsProviderModule,
		          NzTreeViewModule,
		          NzButtonModule,
		          NzInputModule,
		          NzTableModule,
		          NzPopconfirmModule
	          ]
          })
export class DataTreeModule {}
