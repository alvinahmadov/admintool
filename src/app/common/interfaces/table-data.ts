export interface ITableData
{
	name: string;
	value?: string;
}
