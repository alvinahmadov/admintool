import { AfterViewInit, Component, OnInit } from '@angular/core';

import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { takeUntil }                            from 'rxjs/operators';

import { IReferenceTypes, ITreeNode } from 'app/common/interfaces';
import { parseResponse }              from 'app/common/functions';
import { RestApiService }             from 'services/rest-api.service';

type TTreeMap = Map<string, ITreeNode[]>;

@Component({
	           selector:    'fz-type-view',
	           templateUrl: './types-view.component.html',
	           styleUrls:   ['./types-view.component.scss']
           })
export class TypesViewComponent implements OnInit, AfterViewInit
{
	public treeNodesMap: TTreeMap = new Map<string, ITreeNode[]>();
	public currentItems: TTreeMap = new Map<string, ITreeNode[]>();
	public treeData$: Observable<IReferenceTypes>;
	public searchValue$: BehaviorSubject<string> = new BehaviorSubject<string>('');
	public currentPage: number = 0;
	public itemsPerPage: number = 10;
	public totalPages: number;
	private ngDestroy$: Subject<boolean> = new Subject<boolean>();
	
	constructor(
			private apiService: RestApiService
	)
	{}
	
	public ngOnInit(): void
	{
		this.treeData$ = this.apiService.getReferenceTypes();
		this.loadTreeData();
		this.onPageChange(1);
	}
	
	ngAfterViewInit()
	{
	}
	
	onPageChange(currentPage: number)
	{
		this.currentPage = currentPage - 1;
		let items = Array.from(this.treeNodesMap)
		                 .slice(this.currentPage, this.currentPage + this.itemsPerPage + 1);
		
		this.currentItems = new Map(items);
	}
	
	onPageSizeChange(size: number)
	{
		this.itemsPerPage = size;
	}
	
	private loadTreeData()
	{
		let nodeKey = 0;
		let treeNodes: ITreeNode[] = [];
		this.treeData$
		    .pipe(takeUntil(this.ngDestroy$))
		    .subscribe(
				    (ref) =>
				    {
					    parseResponse(ref.ref.mappings, treeNodes, null, ++nodeKey);
					    this.totalPages = (treeNodes.length / this.itemsPerPage) * 10;
					    treeNodes.forEach(node => this.treeNodesMap.set(node.name, node.children));
					    let items = Array.from(this.treeNodesMap)
					                     .slice(this.currentPage, this.currentPage + this.itemsPerPage + 1);
					
					    this.currentItems = new Map(items);
				    }
		    )
	}
}
