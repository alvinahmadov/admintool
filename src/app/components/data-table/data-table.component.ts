import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import { ITableData }                              from 'app/common/interfaces';
import { RestApiService }                          from 'services/rest-api.service';
import { DbService }                               from 'services/db.service';

interface ITableRowData extends ITableData
{
	id: number;
	editable?: boolean;
}

@Component({
	           selector:    'fz-data-table',
	           templateUrl: './data-table.component.html',
	           styleUrls:   ['./data-table.component.scss']
           })
export class DataTableComponent implements OnInit, AfterViewInit
{
	@Input()
	public tableData: ITableData[];
	
	/**
	 * Id of reference item
	 * */
	@Input()
	public refId: string;
	
	/**
	 * Type of reference item
	 * */
	@Input()
	public refType: string;
	
	public isAdding: boolean = false;
	public tableRowData: ITableRowData[] = [];
	
	constructor(
			private apiService: RestApiService,
			private dbService: DbService,
	)
	{ }
	
	ngOnInit(): void
	{}
	
	public ngAfterViewInit(): void
	{
		this.loadTableData();
	}
	
	public startChange(rowId: number)
	{
		let data = this.tableRowData[rowId];
		data.editable = true;
	}
	
	public stopChange(rowId: number)
	{
		let data = this.tableRowData[rowId];
		
		if(this.isAdding && (!data.name || !data.value))
		{
			this.tableRowData.pop();
			this.isAdding = false;
			return;
		}
		
		data.editable = false
	}
	
	public addRow(): void
	{
		this.tableRowData.push({
			                       id:       this.tableRowData.length,
			                       editable: true,
			                       name:     '',
			                       value:    ''
		                       });
		this.isAdding = true;
	}
	
	public saveRow(rowId: number, name: string, value: string)
	{
		this.stopChange(rowId);
		let message: string;
		if(name && value)
		{
			const rowItem = this.tableRowData[rowId];
			const oldValue = rowItem.value;
			const oldName = rowItem.name;
			rowItem.name = name;
			rowItem.value = value;
			
			if(this.isAdding)
			{
				message = `Добавлен элемент '${name}' со значением '${value}' в '${this.refType}/${this.refId}'`;
				this.isAdding = false;
			}
			else
			{
				if(oldValue != value)
				{
					message = `Обновлено значение для элемента '${name}' с '${oldValue}' на '${value}' в '${this.refType}/${this.refId}'`
				}
				else if(oldName != name)
				{
					message = `Обновлено имя элемента '${oldName}' на '${name}' в '${this.refType}/${this.refId}'`
				}
				else
				{
					message = `Обновлены имя элемента '${oldName}' на '${name}' ` +
					          `и значение '${oldValue}' на '${value}' в '${this.refType}/${this.refId}'`
				}
			}
			
			this.dbService.add({ message: message });
			this._save();
		}
	}
	
	public rowCount(value: string)
	{
		if(!value)
			return 1;
		
		const count = value.search(/\n/g);
		if(count <= 0)
			return 1;
		return Math.ceil(count / 5) + 1;
	}
	
	public deleteRow(id: number): void
	{
		const elem = this.tableRowData.find(d => d.id === id);
		this.tableRowData = this.tableRowData.filter(d => d.id !== id);
		this.dbService.add({
			                   message: `Элемент '${elem.name}' со значением '${elem.value}'` +
			                            ` удален из '${this.refType}/${this.refId}'`,
		                   })
		this._save();
	}
	
	private loadTableData()
	{
		if(this.tableData)
		{
			this.tableData
			    .forEach((data, index) =>
					             this.tableRowData
					                 .push({
						                       id:       index,
						                       name:     data.name,
						                       value:    data.value,
						                       editable: false
					                       })
			    );
		}
	}
	
	private _save()
	{
		if(this.tableRowData)
		{
			let rawData: string = "";
			if(this.tableRowData.length > 0)
			{
				const childCount = this.tableRowData.length;
				this.tableRowData.forEach(
						(rowData, index) =>
						{
							rawData += `"${rowData.name}": "${rowData.value}"${index < childCount - 1 ? ',' : ''}`
						}
				)
				const data = JSON.parse(`{${rawData}}`);
				this.apiService.editReference(this.refType, this.refId, data)
				    .subscribe(res => console.log(res));
			}
		}
	}
}
