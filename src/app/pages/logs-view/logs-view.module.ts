import { NgModule }            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { Route, RouterModule } from '@angular/router';

import { NzTimelineModule } from 'ng-zorro-antd/timeline';

import { LogsViewComponent } from './logs-view.component';
import { UserPipe }          from 'app/pipes/user.pipe';
import { DbService }         from 'services/db.service';

const routes: Route[] = [
	{
		path:      '',
		component: LogsViewComponent
	}
]

@NgModule({
	          declarations: [
		          LogsViewComponent,
		          UserPipe,
	          ],
	          imports:      [
		          CommonModule,
		          RouterModule.forChild(routes),
		          NzTimelineModule,
	          ],
	          providers:    [
		          DbService
	          ]
          })
export class LogsViewModule {}
