export interface IFlatNode
{
	expandable: boolean;
	name: string;
	key: string;
	value?: string;
	level: number;
}

export default IFlatNode;
