# Farzoom Reference Book

Project's purpose is to allow user to make changes/update/delete references 
from backend service from browser.

Project uses `yarn` package manager

To set environment variables, please, edit `environments/environment.ts` file. 
The file `environments/environment.prod.ts` is for templating. For production mode,
change `production` value to `true`.

```typescript
export interface Environment
{
	production: boolean;
	
	HTTP_SERVICES_ENDPOINT: string;     // Request endpoint
	HTTPS_SERVICES_ENDPOINT: string;    // Request endpoint for https
	
	HOST: string;                       // Servers host
	PORT: number;                       // Servers host
	LOG_LEVEL: string;                  // Logging level for Logger Module
}
```

## Development server

Install packages by running `yarn install`.

Run `yarn run start` for a dev server. Navigate to `http://localhost:4200/`. The app 
will automatically reload if you change any of the source files.

**_Note:_** In browsers making any request from project is blocked by CORS policies
when project run from `http://localhost:{port}`.

To avoid blocking either run your browser with disabled web security or deploy service to 
server in the same namespace in which your server exists and allow server to serve that origin.

Example for Google Chrome:

```shell
google-chrome --disable-web-security --user-data-dir=/home/.cache/.chrome
```

## Build

Run `yarn run build` to build the project. The build artifacts will be stored in the 
`dist/` directory.

To deploy do next:

```shell
$ yarn run build
$ sudo cp dist/ /usr/share/nginx/html
```

## Docker

Before building docker set environment to production mode

Docker compose:

```shell
sudo docker-compose up --build -d
```

Docker standalone:

```shell
sudo docker build -t farzoom-refbook-image:latest .
sudo docker run -d -p 4200:80 farzoom-refbook-image:latest
```

Author [Alvin Ahmadov](https://gitlab.com/alvinahmadov)

@License [MIT]
