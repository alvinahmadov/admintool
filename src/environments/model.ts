export interface Environment
{
	production: boolean;
	
	HTTP_SERVICES_ENDPOINT: string;
	HTTPS_SERVICES_ENDPOINT: string;
	
	LOCALE: string;
	DATETIME_FORMAT: string;
	TIMEZONE: string;
	
	WEB_CONCURRENCY: number;
	WEB_MEMORY: number;
	HOST: string;
	PORT: number;
}
