import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { Subject, Subscription }                       from 'rxjs';
import { takeUntil }                                   from 'rxjs/operators';
import { ITreeNode }                                   from 'app/common/interfaces';
import { parseResponse }                               from 'app/common/functions';
import { RestApiService }                              from 'services/rest-api.service';

@Component({
	           selector:    'fz-references',
	           templateUrl: './references.component.html',
	           styleUrls:   ['./references.component.scss']
           })
export class ReferencesComponent implements OnInit, OnDestroy, AfterViewInit
{
	private ngDestroy$: Subject<boolean> = new Subject<boolean>();
	public references$: Subscription;
	public references: string[];
	public activeReferences: string[];
	public currentPage: number = 0;
	public itemsPerPage: number = 10;
	public totalItems: number = 0;
	public totalPages: number;
	
	private startPage = () => this.currentPage * this.itemsPerPage
	private endPage = () => (this.currentPage + 1) * this.itemsPerPage;
	
	constructor(
			private apiService: RestApiService
	)
	{
		this.references = [];
	}
	
	public ngOnInit(): void
	{
		if(this.references$)
		{
			this.references$.unsubscribe();
		}
	}
	
	public ngAfterViewInit(): void
	{
		this.loadTreeData();
	}
	
	public ngOnDestroy(): void
	{
		if(this.references$)
		{
			this.references$.unsubscribe();
		}
		this.ngDestroy$.unsubscribe();
	}
	
	private loadTreeData()
	{
		let nodeKey = 0;
		let treeNodes: ITreeNode[] = [];
		this.references$ = this.apiService
		                       .getReferenceTypes()
		                       .pipe(takeUntil(this.ngDestroy$))
		                       .subscribe(
				                       (ref) =>
				                       {
					                       parseResponse(ref.ref.mappings, treeNodes, null, ++nodeKey);
					                       this.totalItems = treeNodes.length;
					                       this.totalPages = (treeNodes.length / this.itemsPerPage) * 10;
					                       treeNodes.forEach(node => this.references.push(node.name));
					                       this.activeReferences = this.references.slice(this.startPage(), this.endPage())
				                       }
		                       );
	}
	
	onPageChange(currentPage: number)
	{
		this.currentPage = currentPage - 1;
		this.activeReferences = this.references.slice(this.startPage(), this.endPage());
	}
	
	onPageSizeChange(size: number)
	{
		this.itemsPerPage = size;
	}
	
}
