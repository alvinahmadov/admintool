import { NgModule }     from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }  from '@angular/forms';

import { NzButtonModule }     from 'ng-zorro-antd/button';
import { NzTableModule }      from 'ng-zorro-antd/table';
import { NzInputModule }      from 'ng-zorro-antd/input';
import { NzPopconfirmModule } from 'ng-zorro-antd/popconfirm';

import { DataTableComponent }  from './data-table.component';
import { IconsProviderModule } from 'app/icons-provider.module';
import { RestApiService }      from 'services/rest-api.service';
import { RouterModule }        from "@angular/router";

@NgModule({
	          declarations: [
		          DataTableComponent
	          ],
	          imports: [
		          CommonModule,
		          NzButtonModule,
		          NzTableModule,
		          NzInputModule,
		          FormsModule,
		          NzPopconfirmModule,
		          IconsProviderModule,
		          RouterModule
	          ],
	          exports:      [
		          DataTableComponent
	          ],
	          providers:    [
		          RestApiService
	          ]
          })
export class DataTableModule {}
