import { AfterViewInit, Component, OnInit } from '@angular/core';
import { from, Observable }                 from 'rxjs';
import { share }                            from 'rxjs/operators';
import { ILogMessage }                      from 'app/common/interfaces';
import { DbService }                        from 'services/db.service';
import { environment as env }               from 'environments/environment';

@Component({
	           selector:    'fz-logs-view',
	           templateUrl: './logs-view.component.html',
	           styleUrls:   ['./logs-view.component.scss']
           })
export class LogsViewComponent implements OnInit, AfterViewInit
{
	readonly datetimeFormat: string = env.DATETIME_FORMAT;
	readonly timezone: string = env.TIMEZONE;
	readonly locale: string = env.LOCALE;
	
	readonly logColors = {
		'LOG':     '#262626',
		'INFO':    '#1088E9',
		'WARN':    '#FF8F41',
		'ERROR':   '#BD33FF',
		'SUCCESS': '#33BD48'
	}
	
	logStream$: Observable<ILogMessage[]>;
	logs: ILogMessage[] = [];
	
	constructor(private dbService: DbService)
	{}
	
	ngOnInit()
	{
		this.logStream$ = from(this.dbService.getAll());
	}
	
	public ngAfterViewInit(): void
	{
		this.logStream$
		    .pipe(share())
		    .subscribe(logs => this.logs = logs);
	}
	
	public logColor(level: string)
	{
		switch(level)
		{
			case "INFO":
				return this.logColors.INFO;
			case "SUCCESS":
				return this.logColors.SUCCESS;
			case "ERROR":
				return this.logColors.ERROR;
			case "WARN":
				return this.logColors.WARN;
			default:
				return this.logColors.LOG;
		}
	}
	
	public circleColor(level: string)
	{
		switch(level)
		{
			case "INFO":
				return "blue";
			case "SUCCESS":
				return "green";
			case "ERROR":
				return "red";
			case "WARN":
				return "gray";
			default:
				return "blue";
		}
	}
	
	public createdAt(log: ILogMessage)
	{
		if(log.createdAt)
			return log.createdAt;
		
		return new Date();
	}
}
